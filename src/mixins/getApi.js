import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import _ from 'lodash'

Vue.use(VueAxios, axios)

export default {
    data () {
        return {
            beforeSort: 'fas fa-caret-down',
            orderKey: '',
            orderBy: 'DESK',
            search: '',
            disableApi: true,
            items: [],
            byGroup: [],
            count: 60
        }
    },
    computed: {
        itemList () {
            let items = _.orderBy(this.items, this.orderKey)
            items =  this.orderBy === 'DESC' ? _.reverse(items) : items;
            if (this.search) {
                items = _.filter(items, (obj) => {
                    for(let keys in obj) {
                        if ( obj[keys] && obj[keys].toString().toUpperCase().indexOf(this.search.toUpperCase()) !== -1 ) {
                            return true
                        }
                    }
                    return false
                });
            }
            return items
        },
        groupsByPosition () {
            return [... new Set(this.users.map(item => item.group))]
        }
    },
    methods: {
        async loadDataWithApi () {
            await this.axios.get(`http://localhost:3000?page=0&limit=${this.count}`)
                .then(e => {
                    if ((this.count) !== (e.data.users.length)) {
                        this.disableApi = true
                    }
                    this.items = e.data.users
                    this.count += 30
                    this.byGroup = _.groupBy(e.data.users, 'group')
                })
        },
        sortBy (key) {
            if(key === this.orderKey) {
                this.orderBy = this.orderBy === 'ASC' ? 'DESC' : 'ASC';
            } else {
                this.orderKey = key;
            }
        }
    },
    async beforeCreate () {
        await this.axios.get(`http://localhost:3000?page=0&limit=30`)
            .then(res => {
                this.items = res.data.users
                this.groups = res.data.groups
                this.disableApi = false
                this.byGroup = _.groupBy(res.data.users, 'group')
            })
            .catch(e => console.log('Что то пошло совершенно не так! ', e))
    }
}