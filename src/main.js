import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Hello from './page/Hello/index'
import List from './page/List/List'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueLodash from 'vue-lodash'

Vue.config.productionTip = false

const options = { name: 'lodash' }

Vue.use(VueRouter)
Vue.use(VueAxios, axios)
Vue.use(VueLodash, options)

const router = new VueRouter({
    mode: 'history',
    base: __dirname,
    routes: [
        {
            path: '/hello',
            name: 'hello',
            component: Hello
        },
        {
            path: '/list',
            name: 'list',
            component: List
        },
    ]
})

new Vue({
    router,
    render: h => h(App)
}).$mount('#app')


require('../src/static/css/bootsrap.min.css')
require('../src/static/Icons/css/all.css')
// require('../node_modules/bootstrap/dist/css/bootstrap.min.css')