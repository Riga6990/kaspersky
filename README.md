# Мини проект kaspersky

## Установка и запуск проекта 
```
npm install => npm run serve
```
## Короткое описание проекта:
Тестовый мини-проект для работы с серверными данными.

Проект написан на Vue.js, содержит в себе адаптивную версию для планшетов и мобильных телефонов.

Работа заключается:

-подключение к БД (в данном случае используется мок-сервер на node.js);

-получение данных, их обработка и вывод на экране;

-сортировка, поиск и разбитие на под группы.

Мок-сервер находится по ссылке: https://gitlab.com/Riga6990/kaspersky-server - для запуска сервера: (node app.js)